//
//  AppDelegate.swift
//  VocalTextEdit
//
//  Created by Joao Cassamano on 11/26/20.
//  Copyright © 2020 Joao Cassamano. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}


//
//  ViewController.swift
//  VocalTextEdit
//
//  Created by Joao Cassamano on 11/26/20.
//  Copyright © 2020 Joao Cassamano. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    
    let speechSynthesizer = NSSpeechSynthesizer()

    @IBOutlet var textView: NSTextView!
    
    var contents: String? {
        get{return textView.string}
        set{textView.string = newValue}
    }
    
    @IBAction func speakButtonClicked(sender: NSButton){
        //var textS = textView.string
        //print("I should speak \(textView.string)")
        if let contents = textView.string, !contents.isEmpty {
            speechSynthesizer.startSpeaking(contents)
        } else{
            speechSynthesizer.startSpeaking("The document is empty.")
        }
    }
    
    @IBAction func stopButtonClicked(sender: NSButton){
        //print("The stop button was clicked")
        speechSynthesizer.stopSpeaking()
    }


}

